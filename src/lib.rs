extern crate csv;

use std::error::Error;
use std::io;
use std::io::{Read, BufRead};
use std::fs::File;

type Input = io::BufReader<Box<Read + 'static>>;
type Matrix = Vec<Vec<Distance>>;

pub fn process(input: &str, output: &str, delimiter: &u8, naive: bool) -> Result<(), Box<Error>> {

    let calls_file = read_calls(input, delimiter)?;

    let mode = match naive {
        true  => MissingDataMode::Naive,
        false => MissingDataMode::PairwiseIgnore
    };

    let distances = hamming(&calls_file, mode);

    let fields = [DistanceFields::Differences,
                  DistanceFields::Valid,
                  DistanceFields:: Percentage];

    for field in &fields {

        let name = match field {
            DistanceFields::Differences => "distance",
            DistanceFields::Valid       => "valid",
            DistanceFields::Percentage  => "percent"
        };

        let out = format!("{base}.{part}.txt", base = output, part = name);

        write_matrix(out, field, &calls_file, &distances, delimiter)?;
    }

    Ok(())
}

fn write_matrix(output: String, field: &DistanceFields, calls_file: &CallsFile,
                distances: &Matrix, delimiter: &u8) -> Result<(), Box<Error>> {

    let mut writer = csv::WriterBuilder::new()
        .delimiter(*delimiter)
        .from_path(output)?;

    writer.write_field("genomes")?;
    writer.write_record(&calls_file.record_names)?;

    for (name, row) in calls_file.record_names.iter().zip(distances) {

        let r = string_vec(row.to_vec(), field);

        writer.write_field(name)?;
        writer.write_record(r)?;

    }

    Ok(())
}

fn hamming(calls_file: &CallsFile, mode: MissingDataMode) -> Matrix {

    let mut matrix = create_matrix(&calls_file.record_count);

    let method: fn(&Vec<i32>, &Vec<i32>) -> Distance = match mode {
        MissingDataMode::PairwiseIgnore => dist_pairwise_drop,
        MissingDataMode::Naive          => dist_naive
    };


    for i in 0..(calls_file.record_count - 1) {

        for j in (i+1)..(calls_file.record_count) {
            let a = (&calls_file.calls)[i].to_vec();
            let b = (&calls_file.calls[j]).to_vec();


            let distance = method(&a, &b);

            matrix[i][j] = distance;
            matrix[j][i] = distance;

        }
    }

    matrix

}

fn dist_pairwise_drop(a: &Vec<i32>, b: &Vec<i32>) -> Distance {

    let mut valid_positions = 0;

    let distance = a.into_iter().zip(b)
        .map(|(i, j) | {
            if *i < 1 || *j < 1 {
                0
            } else {

                valid_positions += 1;
                (i != j) as usize
            }
        })
        .sum();

    Distance {
        differences: distance,
        valid: valid_positions
    }
}

fn dist_naive(a: &Vec<i32>, b: &Vec<i32>) -> Distance {

    let distance = a.into_iter().zip(b)
        .map(|(i, j) | (i != j) as usize)
        .sum();

    Distance {
        differences: distance,
        valid: a.len()
    }
}

fn get_file_length(path: &str) -> usize {

    let input = read_input(path);

    let line_count = input.lines()
        .filter_map(Result::ok)
        .enumerate()
        .last()
        .unwrap()
        .0; // no +1 to account for header

    line_count
}

fn read_input(path: &str) -> Input {

    let file_error_msg = format!("Error: File {} could not be read", &path);

    let input_file = {

             let file = File::open(&path)
                 .expect(&file_error_msg);

             Box::new(file) as Box<Read>
        };

    io::BufReader::new(input_file)

}

fn create_matrix(size: &usize) -> Matrix {

    let size = *size;

    let mut matrix = Vec::with_capacity(size);

    for _ in 0..size {

        let mut row: Vec<Distance> = vec![Distance {differences: 0, valid: 0}; size];

        matrix.push(row);
    }

    matrix
}

fn read_calls(path: &str, delimiter: &u8) -> Result<CallsFile, Box<Error>> {

    let record_count = get_file_length(path);

    let mut rows = Vec::with_capacity(record_count);

    let mut row_identifiers = Vec::with_capacity(record_count);

    let mut reader = csv::ReaderBuilder::new()
        .has_headers(true)
        .delimiter(*delimiter)
        .from_path(path)?;

    for row in reader.deserialize() {


        let raw_record: Vec<String> = row?;

        let mut raw_record = raw_record.iter();

        let mut record = Vec::with_capacity(&raw_record.len() - 1);

        row_identifiers.push(raw_record.next().unwrap().to_string());
        for elem in raw_record {

            let call: i32 = elem.parse()?;

            record.push(call);
        }

        rows.push(record);

    }

    let calls_file = CallsFile {
        record_count: record_count,
        calls: rows,
        record_names: row_identifiers
        };

    Ok(calls_file)
}

fn string_vec(v: Vec<Distance>, field: &DistanceFields) -> Vec<String> {

    v.iter().map(|x: &Distance| {

        match field {
            DistanceFields::Differences => x.differences.to_string(),
            DistanceFields::Valid       => x.valid.to_string(),
            DistanceFields::Percentage  => x.percentage().to_string(),
       }

    }).collect()

}

struct CallsFile {
    record_count: usize,
    calls: Vec<Vec<i32>>,
    record_names: Vec<String>
}

#[derive(Clone, Copy)]
struct Distance {
    differences: usize,
    valid: usize
}

impl Distance {
    fn percentage(&self) -> f32 {

        if self.valid == 0 {
            0.0

        } else {
            (self.differences / self.valid) as f32
        }
    }
}

enum MissingDataMode {
    PairwiseIgnore,
    Naive
}

enum DistanceFields {
    Differences,
    Valid,
    Percentage
}
