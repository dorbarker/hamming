extern crate hamming;

extern crate clap;

use clap::Arg;

use std::path::{Path,PathBuf};

fn main() {

    let args = parse_arguments();

    let input = args.value_of("input").unwrap();

    let output = args.value_of("output").unwrap();

    let delimiter = args.value_of("delimiter")
        .unwrap()
        .as_bytes()
        .first()
        .unwrap();

    let naive = args.is_present("naive");

    hamming::process(input, output, delimiter, naive)
        .expect("Something has gone wrong.");
}

fn parse_arguments() -> clap::ArgMatches<'static > {

    let matches = clap::App::new(env!("CARGO_PKG_NAME"))

        .version(env!("CARGO_PKG_VERSION"))

        .arg(Arg::with_name("delimiter")
            .short("d")
            .long("delimiter")
            .value_name("CHAR")
            .takes_value(true)
            .default_value(",")
            .validator(delimiter_length))

        .arg(Arg::with_name("input")
            .short("i")
            .long("input")
            .value_name("FILE")
            .takes_value(true)
            .required(true)
            .validator(file_exists))

        .arg(Arg::with_name("output")
            .short("o")
            .long("output")
            .value_name("FILE")
            .takes_value(true)
            .required(true)
            .validator(parent_exists))

        .arg(Arg::with_name("naive")
            .short("n")
            .long("naive")
            .multiple(false))

        .get_matches();

    matches
}


fn file_exists(v: String) -> Result<(), String> {

    if Path::new(&v).is_file() {
        Ok(())
    } else {
        Err(format!("Input file {} does not exist.", &v))
    }
}

fn parent_exists(v: String) -> Result<(), String> {

    let path = if *(&v.contains("/")) {
        PathBuf::from(&v)
    } else {
        PathBuf::from("./")
    };

    let parent = path
        .parent()
        .expect("parent");

    if parent.exists() {
        Ok(())
    } else {
        Err(format!("{} does not exist", parent.to_str().unwrap()))
    }
}

fn delimiter_length(v: String) -> Result<(), String> {

    if v.len() == 1 {
        Ok(())
    } else {
        Err(String::from("Delimiter lengths > 1 not yet implemented"))
    }
}
